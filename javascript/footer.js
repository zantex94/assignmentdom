'use strict'

function leftFoot(){
let art = document.getElementById('left');
let img = document.createElement('img'); // create element
img.setAttribute('src', '../picture/Skærmbillede 2021-09-14 kl. 15.43.54.png');
img.setAttribute('alt', 'book icon');
img.setAttribute('width', '64');

let br = document.createElement('br'); 
let h1 = document.createElement('h1');
let txt = document.createTextNode('Always'); // create text
art.appendChild(br); // put on tree
h1.appendChild(txt);
    

let pa = document.createElement('p'); // create element
txt = document.createTextNode("You could have four columns here but you won't. You'll have three like everyone else."); // create text
pa.appendChild(txt); 

if(art.firstElementChild.matches("img")){
    art.setAttribute('style', 'color: white');
    art.innerHTML = "Click Me";
    art.removeChild(img);
    art.removeChild(br);
    art.removeChild(h1);
    art.removeChild(pa);
}else{
    art.innerHTML = "";
    art.appendChild(img);
    art.appendChild(br);
    art.appendChild(h1);
    art.appendChild(pa);
}



}
    


function middleFoot(){
let art = document.getElementById('middle');
let img = document.createElement('img'); // create element
img.setAttribute('src', '../picture/Skærmbillede 2021-09-14 kl. 15.44.24.png');
img.setAttribute('alt', 'book icon');
img.setAttribute('width', '64');

let br = document.createElement('br'); 
let h1 = document.createElement('h1');
let txt = document.createTextNode('Three'); // create text
art.appendChild(br); // put on tree
h1.appendChild(txt);
    

let pa = document.createElement('p'); // create element
txt = document.createTextNode("Have a cog icon above one of these colums if you´re really feeling especially creative"); // create text
pa.appendChild(txt); 




if(art.style.color === "yellow"){
    art.setAttribute('style', 'color: white');
    art.innerHTML = "Click me";
    art.removeChild(img);
    art.removeChild(br);
    art.removeChild(h1);
    art.removeChild(pa);
}else{
    art.setAttribute('style', 'color: yellow');
    art.innerHTML = "";
    art.appendChild(img);
    art.appendChild(br);
    art.appendChild(h1);
    art.appendChild(pa);
}

}

function rightFoot(){
    let art = document.getElementById('right');
    let img = document.createElement('img'); // create element
img.setAttribute('src', '../picture/Skærmbillede 2021-09-14 kl. 15.44.48.png');
img.setAttribute('alt', 'book icon');
img.setAttribute('width', '64');

let br = document.createElement('br'); 
let h1 = document.createElement('h1');
let txt = document.createTextNode('Three'); // create text
art.appendChild(br); // put on tree
h1.appendChild(txt);
    

let pa = document.createElement('p'); // create element
txt = document.createTextNode("The perfekt place to talk about your services. Beacuse co-incidentally, you have three of them.");
pa.appendChild(txt); 




if(art.firstElementChild.matches("img")){
    art.setAttribute('style', 'color: white');
    art.innerHTML = "Click me";
    art.removeChild(img);
    art.removeChild(br);
    art.removeChild(h1);
    art.removeChild(pa);
}else{
    art.innerHTML = "";
    art.appendChild(img);
    art.appendChild(br);
    art.appendChild(h1);
    art.appendChild(pa);
}
   
}