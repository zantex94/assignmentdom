'use strict'

const $ = function (bar) { return document.getElementById(bar); };

let filla = function (ev) {
/* define array for holding stuf*/
    let arri = [];
    let arrh = [];
    let arrt = [];
    arri["leftSecond"] = "../picture/Skærmbillede 2021-09-14 kl. 15.43.54.png";
    arri["centerSecond"] = "../picture/Skærmbillede 2021-09-14 kl. 15.44.24.png";
    arri["rightSecond"] = "../picture/Skærmbillede 2021-09-14 kl. 15.44.48.png";
    arrh["leftSecond"] = "Always";
    arrh["centerSecond"] = "Three";
    arrh["rightSecond"] = "Columns";
    arrt["leftSecond"] = "You could have four columns here but you won't. You'll have three like everyone else.";
    arrt["centerSecond"] = "Have a cog icon above one of these columns if you are really feeling especially creative. ";
    arrt["rightSecond"] = "The perfect place to talk about your services. Because co-incidentally you have three of them.";

    /* creating variable for parameter ev with target.id*/
    let art = $(ev.target.id);
    if (art.innerHTML !== "") {
        while (art.firstChild) {
            art.removeChild(art.firstChild);
        }
    } else {
        let img = document.createElement("img"); // create element
        img.setAttribute("src", arri[ev.target.id]);
        img.setAttribute("alt", "icon");
        img.setAttribute("width", "64");

        let h1 = document.createElement("h1"); // create element
        let txt = document.createTextNode(arrh[ev.target.id]); // create text
        h1.appendChild(txt); // put on tree

        let par = document.createElement("p"); // create element
        txt = document.createTextNode(arrt[ev.target.id]); // create text
        par.appendChild(txt); // put onto tree

        art.appendChild(img);
        art.appendChild(h1);
        art.appendChild(par);
    }
    
    
    
    }
/* use this to handle events on buttons instead of onclick in html*/
    let initialize = function () {
        $("leftSecond").addEventListener("click", filla);
        $("centerSecond").addEventListener("click", filla);
        $("rightSecond").addEventListener("click", filla);
    }
    
    window.addEventListener("load", initialize);